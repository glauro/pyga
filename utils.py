from math import sqrt


def parse_csv(filename):
    """Parses a CSV in the format 'index:int, X:float, Y:float' into a list of lists in
    the following format: [ [X1, Y1], [X2, Y2], ..., [Xn, Yn] ]
    """
    if filename is None:
        raise ValueError()

    with open(filename, 'r') as fi:
        data = []
        for line in fi:
            data.append(eval('[' + line + ']')[1:])

    return data


def euclidean_distance(from_, to_) -> float:
    return sqrt((from_[0] - to_[0]) ** 2 + (from_[1] - to_[1]) ** 2)
