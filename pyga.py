import abc
import typing

import random

import utils

random.seed()

"""
    Implementation of a Genetic Algorithm (GA) for solving the
    Travelling Salesman Problem (TSP)
    
    -----------------------------
    Travelling Salesman Problem (TSP): Given a set of cities and distance between every 
    pair of cities, the problem is to find the shortest possible route that visits every 
    city exactly once and returns to the starting point.
    -----------------------------

    The GA goes as follows:

    1 - Generate random initial population
    2 - Evaluate the fitness of each genome in the population
    3 - If the termination requirements for the GA have been met, go to step 8
    4 - Apply elitism as the genome selection policy and produce the next generation
    5 - Resolve crossover to randomly selected couples of parents
    6 - Resolve mutations
    7 - Go to step 2
    8 - End of algorithm
"""

_genome_count = 0


class Genome:
    MIN_CROSSOVER_LENGTH = 2

    def __init__(self, id_=None, genome=None, evaluation=0.):
        if id_ is None:
            global _genome_count
            _genome_count += 1
            id_ = _genome_count

        self.id = id_

        self._genome = None
        self.genome = genome
        self.evaluation = evaluation

    @property
    def genome(self):
        """
        :return: a shallow copy of the genome data structure 
        """
        return list(self._genome)

    @genome.setter
    def genome(self, value):
        # Default genome data structure is a list of integers
        if value is None:
            value = ()
        self._genome = list(value)

    @property
    def length(self):
        return len(self._genome)

    def initialise_randomly(self, values) -> 'Genome':
        """
        :param values: iterable object providing the possible values in the genome 
        :return: returns this same instance
        """
        new_genome = list(values)
        random.shuffle(new_genome)

        self.genome = new_genome

        return self

    def crossover(self, other, rate, id_=None):
        """
        :param other: the other genome to crossover with
        :param rate: A number in the interval [0, 1.0] indicating the probability of
                crossover.
        :param id_: the id for the offspring genome 
        
        :return: the resulting offspring genome value
        """

        original_genome = self.genome
        if len(original_genome) <= 0:
            return original_genome

        if random.random() < rate:
            min_crossover_len = Genome.MIN_CROSSOVER_LENGTH
            # Resolve crossover (two-point)
            i_start = random.randint(0, len(original_genome) - min_crossover_len)
            i_stop = random.randint(i_start + min_crossover_len, len(original_genome))
            # print("Slice size: " + str(i_stop - i_start))

            slice_ = other.genome[i_start: i_stop]

            offspring_genome = [None] * len(original_genome)
            offspring_genome[i_start: i_stop] = slice_

            missing_values = (x for x in original_genome if x not in slice_)

            i = 0
            for value in missing_values:
                if i == i_start:
                    i = i_stop
                offspring_genome[i] = value
                i += 1
        else:
            offspring_genome = original_genome

        # print("\t\tParents\t" + str(other.genome) + "\t" + str(original_genome))
        # print("\t\tOffsprs\t" + str(offspring_genome) + "\n")
        return offspring_genome

    def mutate(self, rate):
        """
        :param rate: A number in the interval [0, 1.0] indicating the probability of
                mutation for each gene. 
        """
        for i in range(self.length):
            if random.random() < rate:
                f = random.choice(range(self.length))

                # Swap the two values
                mutated = self.genome
                mutated[i], mutated[f] = mutated[f], mutated[i]
                self.genome = mutated


class GenomeFactory:
    def __init__(self, genome_size):
        self.genome_size = genome_size

    def new_random(self, evaluation, id_=None):
        return self.new(genome=None, evaluation=evaluation, id_=id_) \
            .initialise_randomly(range(self.genome_size))

    def new(self, genome, evaluation, id_=None):
        return Genome(genome=genome, evaluation=evaluation, id_=id_)


class ReversingGenome(Genome):
    """A genome which reverses the order of some of its values upon mutating"""

    def __init__(self, id_=None, genome=None, evaluation=0.):
        super().__init__(id_, genome, evaluation)

    def mutate(self, rate):
        for i in range(self.length):
            if random.random() < rate:
                f = random.choice(range(self.length))

                # Reverse
                mutated = self.genome
                mutated[i:f] = reversed(mutated[i:f])
                self.genome = mutated


class ReversingGenomeFactory(GenomeFactory):
    def __init__(self, genome_size):
        super().__init__(genome_size)

    def new(self, genome, evaluation, id_=None):
        return ReversingGenome(genome=genome, evaluation=evaluation, id_=id_)


class ObjectiveFunction:
    @abc.abstractmethod
    def evaluate(self, genome: Genome) -> float:
        """Returns genome's evaluation
            :param genome: the genome to be evaluated
        """
        pass

    @abc.abstractmethod
    def initial(self) -> float:
        """Returns the default evaluation for new genomes"""
        pass


class EuclideanDistance(ObjectiveFunction):
    def __init__(self, map_):
        self.map = map_

    def initial(self) -> float:
        return -1.

    def evaluate(self, genome: Genome) -> float:
        total_distance = 0.
        for i in range(-1, len(genome.genome) - 1):
            total_distance += utils.euclidean_distance(
                self.get_coords(genome.genome[i]), self.get_coords(genome.genome[i + 1]))

        return total_distance

    def get_coords(self, id_):
        return self.map[id_]


class Policy:
    """Selection policy"""

    @abc.abstractmethod
    def select(self, eval_pairs: typing.List[typing.Tuple]) -> typing.List[typing.Tuple]:
        """
        :param eval_pairs: A list of tuples in the format (genome.id, genome.evaluation)
        :return: The selected tuples from eval_pairs
        """
        pass


class ElitistPolicy(Policy):
    def __init__(self, selection_size=1):
        super().__init__()
        self.selection_size = selection_size

    def select(self, eval_pairs):
        eval_pairs.sort(reverse=False, key=lambda t: t[1])
        return eval_pairs[:self.selection_size]


class GAManager:
    """Coordinates the algorithm execution and environment"""

    def __init__(self, genome_factory: GenomeFactory,
                 objective_fun: ObjectiveFunction, policy=None,
                 crossover_rate=0.02, mutation_rate=0.02):
        self.genome_factory = genome_factory
        self.mutation_rate = mutation_rate
        self.crossover_rate = crossover_rate
        self.policy = policy if policy is not None else ElitistPolicy()
        self.population = []
        self.population_size = 0
        self.objective_fun = objective_fun
        self.evaluations = {}

    def initialise_population(self, population_size):
        """Initialises a population of random solutions"""
        self.population_size = population_size
        self.clear_population()

        for i in range(population_size):
            new_genome = self.genome_factory.new_random(self.objective_fun.initial())
            self._add_genome(new_genome)

    def clear_population(self):
        self.population = []
        self.evaluations = {}

    def _add_genome(self, new_genome):
        self.population.append(new_genome)
        self.evaluations[new_genome.id] = new_genome.evaluation

    def evaluate_all(self):
        """Evaluates all genomes in place"""
        for genome in self.population:
            self.evaluate(genome)

    def evaluate(self, genome):
        """Evaluates a single genome in place"""
        genome.evaluation = self.objective_fun.evaluate(genome)
        self.evaluations[genome.id] = genome.evaluation

    def get_selection(self, policy: Policy) -> typing.List[Genome]:
        selected_pairs = policy.select(list(self.evaluations.items()))
        selected_ids = [tup[0] for tup in selected_pairs]

        return [gen for gen in self.population if gen.id in selected_ids]

    def repopulate(self, protected: typing.List[Genome]):
        """Produces the next generation while ensuring 'protected'
            will produce offspring
        """
        parents = list(protected)

        for i in range(self.population_size - len(parents)):
            parents.append(self.population[i])

        random.shuffle(parents)
        self.clear_population()

        for i in range(0, self.population_size - 1, 2):
            for j in range(2):
                offspring = self.genome_factory.new(
                    parents[i + j].crossover(parents[i + 1 - j], self.crossover_rate),
                    self.objective_fun.initial()
                )
                offspring.mutate(self.mutation_rate)
                self._add_genome(offspring)

    def step(self):
        """Creates next generation of genomes and evaluates it"""
        # Executes steps 3 to 7
        self.repopulate(protected=self.get_selection(self.policy))
        self.evaluate_all()
