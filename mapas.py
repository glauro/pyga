import math

from pyga import *
import sys

FILENAME = sys.argv[1]
POPULATION_SIZE = int(sys.argv[2])


def concat(*objs):
    message = ""
    for obj in objs:
        message += " " + str(obj)
    return message


def msg(*objs):
    print(concat(*objs))


def print_best(mgr):
    best = sorted(list(mgr.evaluations.items()), key=lambda t: t[1])

    msg(generation_count, "-", best[0][1], "worst", best[-1][1])


def save_to_file(fi):
    genome = sorted(manager.get_selection(manager.policy), key=lambda t: t.evaluation)[0]
    genome_value = genome.genome
    fi.write(concat(
        generation_count, "-\t\t", genome.evaluation, "\t",
        hash(tuple(genome_value)), "\t", genome_value, "\n"))


map_ = utils.parse_csv("data/" + FILENAME + ".csv")

mutation_rate = 0.005
crossover_rate = 0.05
selection_size = int(math.ceil(POPULATION_SIZE / 4))
manager = GAManager(
    genome_factory=ReversingGenomeFactory(genome_size=len(map_)),
    objective_fun=EuclideanDistance(map_),
    policy=ElitistPolicy(selection_size),
    crossover_rate=crossover_rate,
    mutation_rate=mutation_rate
)

msg("Mutation rate:", mutation_rate)

# Begin algorithm
# ---------------
manager.initialise_population(POPULATION_SIZE)
manager.evaluate_all()

generation_count = 0
print_best(manager)

file = open(FILENAME + "_m" + str(mutation_rate) + "_c" + str(crossover_rate)
            + "_e" + str(selection_size) + "_p" + str(POPULATION_SIZE) + ".txt",
            'w')

save_to_file(file)
while generation_count < 10000:
    batch_size = 50
    for i in range(batch_size):
        manager.step()
    generation_count += batch_size

    msg("\n")
    print_best(manager)

    save_to_file(file)
file.close()
