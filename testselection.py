from pyga import *
import utils


def patt(iterable, *names):
    for obj in iterable:
        msg = ""
        for name in names:
            msg += '\t' + str(getattr(obj, name))
        print(msg)
    print("\n")


policy = ElitistPolicy(5)

map_ = utils.parse_csv("testselection.csv")

manager = GAManager(
    policy=policy,
    objective_fun=EuclideanDistance(map_),
    crossover_rate=0.1,
    mutation_rate=0.15
)

manager.initialise_population(
    population_size=20,
    genome_size=len(map_)
)

manager.evaluate_all()


def print_pop():
    patt(manager.population, 'genome', 'evaluation')


def print_sel():
    patt(manager.get_selection(policy), 'genome')


def step():
    manager.step()
    print("--------------------------")
    print_pop()
    print_sel()


print_pop()
print("\nBest ones")
print_sel()

for i in range(100):
    step()
