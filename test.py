from pyga import *


def concat(*objs):
    message = ""
    for obj in objs:
        message += " " + str(obj)
    return message


def msg(*objs):
    message = concat(*objs)
    print(message)


gen1 = Genome(genome=range(10))
msg("Before mutation", gen1.genome)

gen1.mutate(rate=1.)
msg("After", gen1.genome)

gen2 = Genome(genome=range(9, -1, -1))
msg("Before crossover", gen1.genome, gen2.genome)

msg("Crossover child", gen1.crossover(gen2, rate=1., id_=1337).genome)

gen3 = Genome().initialise_randomly(range(10))
msg("Random initialization", gen3.genome)


class DummyFun(ObjectiveFunction):
    c = 100.0

    def initial(self) -> float:
        DummyFun.c -= 1.0
        return DummyFun.c

    def evaluate(self, genome: Genome) -> float:
        DummyFun.c -= 1.0
        return DummyFun.c + 0.1


mgr1 = GAManager(objective_fun=DummyFun(), policy=ElitistPolicy(3))

mgr1.initialise_population(population_size=6, genome_size=4)


def print_evals(mgr):
    msg("Evaluations:")
    evals = []
    for gen in mgr.population:
        evals.append(gen.evaluation)
    for eval in sorted(evals)[-10:]:
        msg(eval)


print_evals(mgr1)
mgr1.evaluate_all()
print_evals(mgr1)


def get_genome_seqs(mgr: GAManager):
    return [(gen.id, gen.genome) for gen in mgr.population]


msg("Initial", get_genome_seqs(mgr1))

for i in range(3):
    mgr1.step()
    msg(i, [gen.genome for gen in mgr1.get_selection(mgr1.policy)])

msg("Final", get_genome_seqs(mgr1))

map_ = utils.parse_csv("data/mapa100.csv")

msg("\n\n\n", "Testing real case\n ================= ")

mgr_euc = GAManager(EuclideanDistance(map_), ElitistPolicy(50),
                    crossover_rate=0.25, mutation_rate=0.1)

mgr_euc.initialise_population(population_size=200, genome_size=100)
mgr_euc.evaluate_all()


def print_best(mgr):
    for v in sorted(mgr.get_selection(mgr.policy), key=lambda t: t.evaluation)[0:1]:
        msg(v.id, "-", v.evaluation, v.genome)


print_best(mgr_euc)

generation_count = 0


def save_to_file():
    with open('mapa100.txt', 'a') as fi:
        genome = \
            sorted(mgr_euc.get_selection(mgr_euc.policy), key=lambda t: t.evaluation)[0]
        fi.write(concat(
            generation_count, "-\t\t", genome.evaluation, "\t", genome.genome, "\n"))


save_to_file()
while True:
    batch_size = 200
    for i in range(batch_size):
        mgr_euc.step()
    generation_count += batch_size

    if mgr_euc.mutation_rate < 0.1:
        mgr_euc.mutation_rate += 0.02

    msg("\n")
    print_best(mgr_euc)

    save_to_file()

msg("Best answer", mgr_euc.get_selection(mgr_euc.policy)[0].evaluation)
